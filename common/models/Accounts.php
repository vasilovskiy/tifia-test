<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%accounts}}".
 *
 * @property int $id
 * @property int $client_uid
 * @property int $login
 *
 * @property Users $clientU
 * @property Trades[] $trades
 */
class Accounts extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%accounts}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['client_uid', 'login'], 'integer'],
            [['client_uid'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['client_uid' => 'client_uid']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'client_uid' => 'Client Uid',
            'login' => 'Login',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientU()
    {
        return $this->hasOne(Users::className(), ['client_uid' => 'client_uid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrades()
    {
        return $this->hasMany(Trades::className(), ['login' => 'login']);
    }
}