<?php

use yii\db\Migration;

/**
 * Class m190920_001844_add_keys_to_tables
 */
class m190920_001844_add_keys_to_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createIndex('idx-users-client_uid', '{{%users}}', 'client_uid');
        $this->createIndex('idx-users-partner_id', '{{%users}}', 'partner_id');
        $this->createIndex('idx-accounts-client_uid', '{{%accounts}}', 'client_uid');
        $this->createIndex('idx-accounts-login', '{{%accounts}}', 'login');
        $this->createIndex('idx-trades-login', '{{%trades}}', 'login');

        $this->alterColumn('{{%accounts}}', 'client_uid', $this->integer(11));
        $this->alterColumn('{{%accounts}}', 'login', $this->integer(11));

        $this->addForeignKey(
            'fk-accounts-client_uid',
            '{{%accounts}}',
            'client_uid',
            '{{%users}}',
            'client_uid',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-trades-login',
            '{{%trades}}',
            'login',
            '{{%accounts}}',
            'login',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-trades-login', '{{%trades}}');
        $this->dropForeignKey('fk-accounts-client_uid', '{{%accounts}}');

        $this->dropIndex('idx-trades-login', '{{%trades}}');
        $this->dropIndex('idx-accounts-login', '{{%accounts}}');
        $this->dropIndex('idx-accounts-client_uid', '{{%accounts}}');
        $this->dropIndex('idx-users-partner_id', '{{%users}}');
        $this->dropIndex('idx-users-client_uid', '{{%users}}');
    }
}
