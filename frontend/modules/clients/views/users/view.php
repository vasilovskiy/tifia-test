<?php

use yii\helpers\Html;
use yii\helpers\Json;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Users */
/* @var $partnerListJson string */
/** @var $countPartners int */
/** @var $countFirstLine int */
/** @var $countDepth int */
/** @var $periodVolume int */
/** @var $periodProfit int */
/* @var $chart array */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Clients', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);

$this->registerCssFile('https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/themes/default/style.min.css');
$this->registerJsVar('partnerListJson', $partnerListJson);
$this->registerJs("

    $('.structure-tree').on('loaded.jstree', function(e, data){
        $('[data-toggle=popover]').popover();
    }).on('open_node.jstree', function(e, data){
        $('[data-toggle=popover]').popover();
    });
   
    console.log(partnerListJson);
   
    $('#jstree').jstree({
    	\"core\": {
	        \"themes\":{
	            \"icons\":false
	        },
			'data': partnerListJson
	    },
    });
");

?>
    <div class="users-view">

        <h1>Client <?= $model->client_uid ?></h1>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'client_uid',
                'email:email',
                'gender',
                'fullname',
                'country',
                'region',
                'city',
                'address',
                'partner_id',
                'reg_date',
                'status',
            ],
        ]) ?>

    </div>

    <h4>Количество прямых рефералов : <?= $countFirstLine ?></h4>
    <h4>Количество всех рефералов : <?= $countPartners ?></h4>
    <h4>Количество уровней реферальной сетки : <?= $countDepth ?></h4>

    <div id="jstree"></div>

    <h4>Суммарный объем за период времени : <?= $periodVolume ?></h4>
    <h4>Прибыльность за период времени <?= $periodProfit ?></h4>

<?= $this->render('profit-chart', [
    'chart' => $chart,
    'client' => $model,
]) ?>