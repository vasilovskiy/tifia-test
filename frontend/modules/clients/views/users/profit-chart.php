<?php

use frontend\assets\ChartAsset;

/* @var $this yii\web\View */
/* @var $datasets */
/* @var $labels */
/* @var $title */
/* @var $type */
/* @var $startDate */
/* @var $endDate */
/* @var $chart array */
/* @var $client common\models\Users */

ChartAsset::register($this);

$this->registerJsVar('title', 'Profit chart');
$this->registerJsVar('labels', $chart['labels']);
$this->registerJsVar('datasets', $chart['datasets']);
$this->registerJsVar('startDate', $chart['startDate']);
$this->registerJsVar('endDate', $chart['endDate']);

$this->registerJs(<<<JS

var ctx = document.getElementById('myChart').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: labels,
        datasets: datasets
    },
    options: {
        scales: {
            yAxes: [{
                type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
                display: true,
                position: 'left',
                id: 'y-axis-1',
                ticks: {
                    beginAtZero: true
                }
            }, {
                type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
                display: true,
                position: 'right',
                id: 'y-axis-2',
                // grid line settings
                gridLines: {
                    drawOnChartArea: false, // only want the grid lines for one axis to show up
                },
                ticks: {
                    beginAtZero: true
                }
            }],
        },
        responsive: true,
        hoverMode: 'index',
        stacked: false,
        legend: {
            position: 'top',
        },
        title: {
            display: true,
            text: title
        },
        tooltips: {
            position: 'average',
            mode: 'index',
            intersect: false,
        }
    }
});

$(function() {
  $('input[name="daterange"]').daterangepicker({
    startDate: startDate, 
    endDate: endDate,
    opens: 'left',
    showDropdowns: true,
    locale: {
        "format": 'YYYY-MM-DD',
        "weekLabel": "W",
        "firstDay": 1,
    }
  });
});

setTimeout(function() {
    $(document).on('change', '#filter-form select, #filter-form input', function() {
        $('#filter-form').submit();
    });
}, 750);
JS
);

?>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">
                </h3>
            </div>
            <div class="box-body chart-responsive">
                <div class="row">
                    <form id="filter-form" action="<?= Yii::$app->request->url ?>" method="get">
                        <div class="col-sm-4">
                            <input type="hidden" name="id" value="<?= $client->id ?>"/>
                            <input type="text" name="daterange" class="form-control"/>
                        </div>
                    </form>
                </div>
                <div class="chart">
                    <canvas id="myChart" style="height: 400px; width: 788px;"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>