<?php


namespace frontend\modules\clients\components;

use common\models\Users as Client;
use DateInterval;
use DatePeriod;
use DateTime;
use Yii;
use yii\base\Component;
use yii\db\Query;

class ProfitChart extends Component
{
    protected $dateRange;

    /** @var Client $client */
    protected $client;
    /** @var DateTime $startDate */
    protected $startDate;
    /** @var DateTime $endDate */
    protected $endDate;
    /** @var DatePeriod $days */
    protected $days;

    /**
     * Chart constructor.
     * @param $params
     * @param Client $client
     */
    public function __construct($params, Client $client)
    {
        $this->dateRange = $params['daterange'] ?? false;

        $this->client = $client;

        parent::__construct();
    }

    /**
     * @throws \Exception
     */
    public function init()
    {
        if ($this->dateRange) {
            $start = trim(explode(' - ', $this->dateRange)[0]);
            $end = trim(explode(' - ', $this->dateRange)[1]);

            $this->startDate = new DateTime($start);
            $this->endDate = new DateTime($end);
        } else {
            $this->startDate = new DateTime('- 180 days');
            $this->startDate->setTime(0, 0, 0);

            $this->endDate = new DateTime('now');
            $this->endDate->setTime(0, 0, 0);
        }

        $interval = new DateInterval('P1D');

        $this->endDate->add($interval);

        $this->days = new DatePeriod($this->startDate, $interval, $this->endDate);

        $this->endDate->sub($interval);
    }

    public function getStartDate(): string
    {
        return Yii::$app->formatter->asDate($this->startDate, 'php:Y-m-d');
    }

    public function getEndDate(): string
    {
        return Yii::$app->formatter->asDate($this->endDate, 'php:Y-m-d');
    }

    public function getLabels(): array
    {
        $labels = [];

        foreach ($this->days as $day) {
            $labels[] = \Yii::$app->formatter->asDate($day, 'php:j M Y');
        }

        return $labels;
    }

    public function getChartDatasetsArray(): array
    {
        $startDate = $this->startDate->format('Y-m-d');
        $endDate = $this->endDate->format('Y-m-d');

        $cacheKey = "dataProfit-{$this->client->client_uid}-{$startDate}-{$endDate}";

        if (Yii::$app->cache->get($cacheKey)) {
            return Yii::$app->cache->get($cacheKey);
        }

        $profitQuery = $this->client->getTrades()
            ->select('DATE(close_time) as close_time, profit, volume, coeff_h, coeff_cr')
            ->andWhere(['>=', 'DATE(close_time)', $startDate])
            ->andWhere(['<=', 'DATE(close_time)', $endDate]);

        $arrayDate = $this->getArrayDate();

        $dataSet = $this->getValuesByDays($profitQuery, $arrayDate);

        $dataSetProfit = new ChartDataset(
            'RUB',
            $dataSet['profit'],
            'rgba(54, 162, 235, 0.8)',
            'rgba(54, 162, 235, 0.8)',
            1,
            '',
            'y-axis-1',
            false
        );

        $dataSetVolume = new ChartDataset(
            'Volume',
            $dataSet['volume'],
            'rgba(75, 192, 192, 1)',
            'rgba(75, 192, 192, 1)',
            1,
            '',
            'y-axis-2',
            false
        );

        $data = [$dataSetVolume, $dataSetProfit];

        Yii::$app->cache->set($cacheKey, $data);

        return $data;
    }

    private function getValuesByDays(Query $profitQuery, $data)
    {
        $volume = $data;
        $profit = $data;

        foreach ($profitQuery->batch(500) as $profits) {
            foreach ($profits as $item) {
                foreach ($data as $date => $day) {
                    if ($date === $item['close_time']) {
                        $profit[$date] += $item['profit'];
                        $volume[$date] += $item['volume'] * $item['coeff_h'] * $item['coeff_cr'];
                    }
                }
            }
        }

        foreach ($profit as $key => $value) {
            $profit[$key] = round($value, 2);
        }

        foreach ($volume as $key => $value) {
            $volume[$key] = round($value, 2);
        }

        return [
            'volume' => array_values($volume),
            'profit' => array_values($profit)
        ];
    }

    private function getArrayDate()
    {
        $data = [];

        foreach ($this->days as $day) {
            $date = Yii::$app->formatter->asDate($day, 'php:Y-m-d');
            $data[$date] = 0;
        }

        return $data;
    }
}
