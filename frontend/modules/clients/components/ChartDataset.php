<?php


namespace frontend\modules\clients\components;


class ChartDataset
{
    public $label;

    public $data = [];

    public $backgroundColor;

    public $borderColor;

    public $borderWidth;

    public $yAxisID;

    public $cubicInterpolationMode;

    public function __construct(
        string $label,
        array $data,
        string $backgroundColor = 'rgba(255, 99, 132, 0.8)',
        string $borderColor = 'rgba(255, 99, 132, 1)',
        int $borderWidth = 1,
        string $cubicInterpolationMode = '',
        string $yAxisID = ''
    )
    {
        $this->label = $label;
        $this->data = $data;
        $this->backgroundColor = $backgroundColor;
        $this->borderColor = $borderColor;
        $this->borderWidth = $borderWidth;
        $this->cubicInterpolationMode = $cubicInterpolationMode;
        $this->yAxisID = $yAxisID;
    }
}