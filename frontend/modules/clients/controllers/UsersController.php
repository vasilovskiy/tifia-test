<?php

namespace frontend\modules\clients\controllers;

use common\models\Users;
use frontend\modules\clients\components\ProfitChart;
use frontend\modules\clients\services\UsersService;
use frontend\modules\clients\models\UsersSearch;
use yii\web\NotFoundHttpException;
use yii\web\Controller;
use Yii;

/**
 * UsersController implements the CRUD actions for Users model.
 */
class UsersController extends Controller
{
    /**
     * @var UsersService
     */
    public $usersService;

    /**
     * UsersController constructor.
     * @param $id
     * @param $module
     * @param UsersService $usersService
     */
    public function __construct($id, $module, UsersService $usersService)
    {
        $this->usersService = $usersService;

        parent::__construct($id, $module);
    }

    /**
     * Lists all Users models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UsersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Users model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function actionView($id)
    {
        $queryParams = Yii::$app->request->queryParams;

        $client = $this->findModel($id);

        $listPartners = [];
        $countDepth = 0;

        $this->usersService::getInstance()->getPartner($client, $listPartners, $countDepth);

        $partnerListJson = $this->getPartnerListJson($client, $listPartners);
        $countPartners = count($listPartners);
        $countFirstLine = $client->getPartners()->count();

        $profitChart = new ProfitChart($queryParams, $client);

        $labels = $profitChart->getLabels();
        $datasets = $profitChart->getChartDatasetsArray();
        $startDate = $profitChart->getStartDate();
        $endDate = $profitChart->getEndDate();

        $periodVolume = array_sum($datasets[0]->data);
        $periodProfit = array_sum($datasets[1]->data);

        return $this->render('view', [
            'model' => $client,
            'countFirstLine' => $countFirstLine,
            'countPartners' => $countPartners,
            'countDepth' => $countDepth,
            'partnerListJson' => $partnerListJson,
            'chart' => [
                'labels' => $labels,
                'datasets' => $datasets,
                'startDate' => $startDate,
                'endDate' => $endDate,
            ],
            'periodVolume' => $periodVolume,
            'periodProfit' => $periodProfit
        ]);
    }

    public function getPartnerListJson(Users $client, $partners)
    {
        $tree = [];

        /** @var Users $partners */
        foreach ($partners as $partner) {
            $tree[] = [
                'id' => $partner->client_uid,
                'parent' => $partner->partner_id === $client->client_uid ? '#' : $partner->partner_id,
                'text' => $this->renderPartial('_tree_item', [
                    'model' => $partner,
                ]),
                'a_attr' => [
                    'class' => 'structure-tree__item',
                ],
            ];
        }

        return $tree;
    }

    /**
     * Finds the Users model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return array|\yii\db\ActiveRecord
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $model = Users::find()->alias('u')->joinWith('partners p')->where(['u.id' => $id])->one();

        if ($model) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
