<?php


namespace frontend\modules\clients;

/**
 * Clients module definition class
 * @package frontend\modules\clients
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'frontend\modules\clients\controllers';
}