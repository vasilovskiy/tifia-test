<?php


namespace frontend\modules\clients\services;


use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;
use yii\di\NotInstantiableException;
use yii\helpers\ArrayHelper;
use common\models\Users;

class UsersService extends Component
{
    /**
     * @return object
     * @throws InvalidConfigException
     * @throws NotInstantiableException
     */
    public static function getInstance()
    {
        return Yii::$container->get(static::class);
    }

    /**
     * Get array partners
     * @param Users $user
     * @param $list
     * @param $depth
     * @param int $cur_d
     */
    public function getPartner(Users $user, &$list, &$depth, $cur_d = 0)
    {
        if ($user->partners) {
            $list = ArrayHelper::merge($list, $user->partners);

            $cur_d += 1;

            foreach ($user->partners as $partner) {
                static::getPartner($partner, $list, $depth, $cur_d);
            }

        } else {

            if ($cur_d > $depth) {
                $depth = $cur_d;
            }

            return;
        }
    }
}