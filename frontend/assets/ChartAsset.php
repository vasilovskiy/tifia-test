<?php

namespace frontend\assets;

use yii\web\AssetBundle;


class ChartAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'css/daterangepicker.css'
    ];

    public $js = [
        'js/chart/chart.js',
        'js/chart/raphael.js',
        'js/chart/morris.min.js',
        'js/moment.min.js',
        'js/daterangepicker.js',
    ];

    public $depends = [
        'frontend\assets\AppAsset',
    ];
}